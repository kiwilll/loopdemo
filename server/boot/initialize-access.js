var Promise = require('bluebird');

module.exports = function(app, callback) {
  app.datasources.loopdemo.autoupdate(['Role', 'RoleMapping'], () => {
    var Role = app.models.Role;
    var RoleMapping = app.models.RoleMapping;

    const role = {
      name: 'admin'
    };

    Role.findOrCreate({
      where: role
    }, role).then(entity => {
      const roleMapping = {
        principalType: RoleMapping.USER,
        principalId: 10,
        roleId: entity[0].id
      };

      return RoleMapping.findOrCreate({
        where: roleMapping
      }, roleMapping);
    }).then(principal => {
      callback();
    }).catch((err) => {
      callback(err);
    });
  });
};
