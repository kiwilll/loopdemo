'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var request = require('request');

class Mailer{
  constructor(config={}){
    this.url = config.url;
    this.options = {
      appid: config.appid,
      from: config.from,
      to: config.to,
      signature: config.appkey
    };
  }

  send(subject, content, callback) {
    this.options.subject = subject;
    this.options.text = content;
    request.post({
      uri: this.url,
      json: true,
      body: this.options
    }, (err, res, body)=>{
      if (err) return callback(err);
      callback(null, body);
    })
  }
}

exports.default = Mailer;
