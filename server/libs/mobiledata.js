var request = require('request');

class MobileData{
  constructor(config={}){
    this.url = config.url;
    this.options = {
      appid: config.appid,
      signature: config.appkey
    };
  }

  charge(target, amount, callback) {
    this.options.to = target;
    this.options.cm = amount.cm;
    this.options.cu = amount.cu;
    this.options.ct = amount.ct;
    request.post({
      uri: this.url,
      json: true,
      body: this.options
    }, (err, res, body)=>{
      if (err) return callback(err);
      callback(null, body);
    })
  }
}

exports.default = MobileData;