class LV{
  getAll(){
    let lvs = [
      0,
      200,
      500,
      1000,
      2000,
      3000,
      4000,
      5000,
      6000
    ];
    return lvs;
  }

  getLv(exp){
    let lvs = this.getAll();
    let lv = 0;
    for(let i = 0; i < lvs.length; i++) {
      if(exp >= lvs[i]) {
        lv = i;
      } else {
        return lv;
      }
    }
    return lv;
  }
}

exports.default = LV;