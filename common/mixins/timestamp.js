var _ = require('lodash');

module.exports = (Model, options) => {
  Model.defineProperty('created_at', {
    type: Date,
    defaultFn: 'now',
    description: '创建时间'
  });

  Model.defineProperty('updated_at', {
    type: Date,
    defaultFn: 'now',
    description: '更新时间'
  });

  Model.observe('before save', (ctx, next)=> {
    if (ctx.data && Model.modelName === 'item' && _.isNumber((ctx.data.views || ctx.data.thumbsups))) {
      return next();
    }

    if (ctx.data && Model.modelName === 'figure' && _.isNumber(ctx.data.download)) {
      return next();
    }

    if (ctx.instance) {
      ctx.instance.updated_at = ctx.instance.updated_at || Date.now();
    } else {
      ctx.data.updated_at = ctx.data.updated_at || Date.now();
    }

    next();
  });
};

