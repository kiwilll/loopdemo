module.exports = function(Model, options = {}) {

  const {name, ref} = options;

  Model.prototype[name] =function(filter, callback) {
    const Comment = Model.app.models.comment;
    const {skip, limit} = filter || {};

    const userScope = {
      relation: 'user',
      scope: {
        fields: {id: 1, nickname: 1, headimg: 1}
      }
    };

    const replyeeScope = {
      relation: 'replyee',
      scope: {
        fields: {id: 1, nickname: 1, headimg: 1}
      }
    }

    let query = {
      where: {
        and: [
          {[ref]: this.id},
          {commentId: null }
        ]
      },
      include: [
        {
          relation: 'nestedComments',
          scope: {
            include: [userScope, replyeeScope],
            order: 'created_at DESC'
          }
        },
        userScope,
        replyeeScope
      ],
      order: 'created_at DESC'
    };

    skip && (query.skip = skip);
    limit && (query.limit =limit);

    Comment.find(query, callback);
  };

  Model.remoteMethod(options.name, {
    accepts: {
      arg: 'filter',
      type: 'object',
      description: 'skip/limit'
    },
    isStatic: false,
    http: {verb: 'get'},
    returns: {root: true, type: 'object'}
  });
};
