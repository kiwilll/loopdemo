var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Item) {
  RemoteRouting(Item, {only: [
    '@find',
    '@findById',
    '@count',

    '__count__collectors',

    '__get__images',
    '__count__images',

    '__get__tags',

    '__get__thumbsup_users',
    '@__get__item_comments',,
    '@create'
  ]})
}

