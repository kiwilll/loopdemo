var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Category) {
  RemoteRouting(Category, {only: [
    '@find',
    '__get__items',
    '__get__tags',
    '__exists__tags',
    '__link__tags',
    '__unlink__tags',
    '@create'
  ]});
}
