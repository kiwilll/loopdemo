var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Misc) {
  RemoteRouting(Misc, {only: [
    '@find',
    '@findById',
    '__get__activity'
  ]})
}
