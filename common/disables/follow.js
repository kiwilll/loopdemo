var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Follow) {
  let isStatic = [
    'find',
    'upsert',
    'exists',
    'findById',
    'createChangeStream',
    'deleteById',
    'count',
    'findOne',
    'updateAll'
  ];

  let nonStatic = [
    'updateAttributes',
    '__get__followee',
    '__get__follower'
  ];

}
