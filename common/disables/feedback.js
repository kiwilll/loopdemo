var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Feedback) {
  RemoteRouting(Feedback, {only: [
    '@create'
  ]});
}
