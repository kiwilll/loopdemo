var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Message) {
  RemoteRouting(Message, {only: [
    '@deleteById',
    '@count'
  ]})
}
