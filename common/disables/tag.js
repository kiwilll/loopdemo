var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Tag) {
  RemoteRouting(Tag, {only: [
    '__get__categories',
    '__unlink__categories',
    '__get__items'
  ]})
}
