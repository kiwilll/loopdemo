var RemoteRouting = require('loopback-remote-routing');

module.exports = function(User) {
  RemoteRouting(User, {only: [
    '@findById',
    '@login',
    '@logout',
    'updateAttributes',

    '__get__collected_items',
    '__count__collected_items',
    '__link__collected_items',
    '__unlink__collected_items',

    '__get__followees',
    '__count__followees',

    '__get__followers',
    '__count__followers',

    '__get__items',

    '__link__thumbsup_activities',

    '__get__thumbsup_items',
    '__link__thumbsup_items',
    '__unlink__thumbsup_items',

    '__get__collected_miscs',
    '__count__collected_miscs',
    '__link__collected_miscs',
    '__unlink__collected_miscs',

    '__get__collected_figures',
    '__count__collected_figures',
    '__link__collected_figures',
    '__unlink__collected_figures',

    '__create__product_comments',

    '__create__comments',
    '__create__backgrounds',
    '__destroyById__backgrounds',

    '__get__account',
    'getLv',
    '@create'
  ]})
}

