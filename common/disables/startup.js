var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Startup) {
  RemoteRouting(Startup, {only: [
    '@find',
    '@findById'
  ]})
}
