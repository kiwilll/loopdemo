var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Thumbsup) {
  RemoteRouting(Thumbsup, {only: [
    '@count'
  ]})
}

