var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Figure) {
  RemoteRouting(Figure, {only: [
    '@find',
    '@findById'
  ]});
}

