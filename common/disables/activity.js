var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Activity) {
  RemoteRouting(Activity, {only: [
    '@find',
    '@findById',
    '@create'
  ]})
}
