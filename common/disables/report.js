var RemoteRouting = require('loopback-remote-routing');

module.exports = function (Report) {
  RemoteRouting(Report, {only: [
    '@create'
  ]})
}
