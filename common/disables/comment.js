var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Comment) {
  RemoteRouting(Comment, {only: [
    '@find',
    '@create',
    '@deleteById',
    '__get__activity',
    '@count',
    '@create'
  ]})
}
