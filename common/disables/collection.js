var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Collection) {
  RemoteRouting(Collection, {only: [
    '@count',
    '@create'
  ]});
}
