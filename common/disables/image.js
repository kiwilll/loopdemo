var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Image) {
  RemoteRouting(Image, {only: [
    '@find',
    '@create',
    '@count'
  ]})
}
