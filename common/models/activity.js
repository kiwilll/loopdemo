var DisableActivityRemote = require('../disables/activity.js');
var loopback = require('loopback');
var Promise = require('bluebird');
var bunyan = require('bunyan');

let logger = bunyan.createLogger({name: 'ZCM'});

module.exports = function(Activity) {
  DisableActivityRemote(Activity);

  function thumbsupOwner(ctx, instance, next){
    const Thumbsup = loopback.getModel('thumbsup');
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;

    if (userId) {
      Promise.resolve(instance).map(activity=>{
        return Thumbsup.count({activityId: activity.id, userId}).then(count=>{
          activity.thumbsupOwner = count ? true : false;
          return activity;
        });
      }).then(()=>next());
    } else {
      instance.forEach(activity=> activity.thumbsupOwner = false);
      next();
    }
  }

  Activity.afterRemote('findById', function thumbsupUser(ctx, instance, next){
    const Thumbsup = loopback.getModel('thumbsup');
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;

    if (userId) {
      Thumbsup.count({activityId: instance.id, userId}).then(count=>{
        instance.thumbsupOwner = count ? true : false;
        next();
      }).catch(()=>next());
    } else {
      instance.thumbsupOwner = false;
      next();
    }
  });

  //hot fix for apple store version 1.0.0
  Activity.beforeRemote('find', (ctx, instance, next)=>{
    if (/ZhongCaoMei\/1.0/.test(ctx.req.headers['user-agent'])) {
      ctx.req.query.filter.where.type = 'pane';
      delete ctx.req.query.filter.skip;
      delete ctx.req.query.filter.limit;
      delete ctx.req.query.filter.offset;
      next();
    } else {
      next();
    }
  });

  Activity.afterRemote('find', (ctx, instance, next)=>{
    if (/ZhongCaoMei\/1.0/.test(ctx.req.headers['user-agent'])) {
      Activity.find({
        where: {type: 'scroll', active: true},
        order: 'orderId DESC'
      }).then(scrolls=>{
        ctx.result = ctx.result.filter(activity=>{
          if (activity.type != 'scroll') {
            return activity;
          }
        });
        ctx.result = [...ctx.result, ...scrolls]
        next();
      }).catch(next);
    } else {
      next();
    }
  });

  Activity.afterRemote('find', thumbsupOwner);

  //return activities following specified order
  Activity.afterRemote('find', (ctx, instance, next)=>{
    let query = ctx.req.query, ids;
    if(query.filter) {
      const filter = query.filter;
      if(filter.where && filter.where.id && (ids = filter.where.id.inq) && !!ids.length) {
        ctx.result = ids.map(id=> {
          return ctx.result.filter(activity=> activity.id == id)[0];
        });
      }
    }

    next();
  });


  // Activity.prototype.increaseViews = function(callback) {
  //   callback(null, {views: this.views});
  // };

  // Activity.beforeRemote('prototype.increaseViews', (ctx, instance, next)=> {
  //   instance && instance.updateAttribute('views', ++instance.views)
  //     .then(()=> next()).catch(next);
  // });

  // Activity.remoteMethod('increaseViews', {
  //   http: {verb: 'post', status: 200},
  //   description: 'increase activity views',
  //   accepts: [],
  //   isStatic: false,
  //   returns: { root: true, type: 'number' }
  // });

  Activity.increaseViews = function(id, callback) {
    Activity.findOne({where: {id: id}}).then(ins=> {
      if(!ins) return callback(null, {views: 0});

      return ins.updateAttribute('views', ++ins.views).then(ins1=>{
        callback(null, {views: ins1.views});
      });
    }).catch(callback);
  };

  Activity.remoteMethod('increaseViews', {
    http: {path: '/:id/increaseViews',verb: 'post', status: 200},
    description: 'instance activity views',
    accepts: {arg: 'id', type:'number', http: {source: 'path'}, required: true},
    returns: {root: true, type: 'number'}
  });

  Activity.afterRemote('search', thumbsupOwner);

  /*
   * return activities that text or description matches keyword,
   * ordering by updated_at time DESC
   *
   * @params {String} keyword
   * @params {Date} updatedAt
   * @callback {Function} callback
   */
  Activity.search = (keyword, updatedAt, callback)=>{
    if (!updatedAt) {
      updatedAt = new Date().toISOString();
    }

    let filter = {
      order: ['updated_at DESC'],
      limit: 10,
      where: {
        and: [
          { updated_at: {lt: new Date(updatedAt)} },
          { active: true }
        ]
      }
    };

    if (keyword) {
      filter.where.and.push({or: [
        {text: {regexp: `/${keyword}/i`}},
        {description: {regexp: `/${keyword}/i`}}
      ]});
    }

    let Comment = loopback.getModel('comment');

    Activity.find(filter).then(activities=>{
      return Promise.resolve(activities).map(a=>{
        return Comment.count({or:[{instModel: 'activity', instId: a.id}, {activityId: a.id}]}).then(count=>{
          a.commentCount = count;
          return a;
        });
      });
    }).then(a=> {
      callback(null, a);
    }).catch(err=>{
      logger.error(err);
      return callback(err);
    });
  };

  Activity.remoteMethod('search', {
    accepts: [ {
      arg: 'keyword',
      type: 'string',
      http: {source: 'query'},
      description: '搜索关键字'
    }, {
      arg: 'updatedAt',
      type: 'date',
      http: {source: 'query'},
      description: '更新时间戳'
    } ],
    http: {verb: 'get'},
    returns: { root: true, type: ['object']}
  });

};
