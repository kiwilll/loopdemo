var DisableThumbsupRemote = require('../disables/thumbsup.js');
var loopback = require('loopback');
var lodash = require('lodash');
var Promise = require('bluebird');

// let options = {};

// if (process.env.NODE_ENV === 'staging') {
   // options.redis = {auth: 'Fanfan123'};
  // options.redis = {auth: '5eb280e28065c26571b8e876ab4e0e4a'}
// }

// if (process.env.NODE_ENV === 'production') {
//   options.redis = {
//     port: 6379,
//     host: '120.55.190.89',
//     auth: '5eb280e28065c26571b8e876ab4e0e4a'
//   }
// }

// let queue = kue.createQueue(options);
let FIVE_MINUTES = 15 * 60 * 1000;

module.exports = function(Thumbsup) {
  let User = loopback.getModel('user');

  // queue.process('publish', 5, (job, done)=>{
  //   let {itemId} = job.data;
  //   let filter = null;

  //   if (process.env.NODE_ENV === 'staging') {
  //     filter = { where: { id: { inq: [1,2,3,4,6,9, 10, 11, 12, 13, 14, 15]} } };
  //   }

  //   if (process.env.NODE_ENV === 'production') {
  //     let usernames = [];
  //     for(let i = 100; i <= 150; i++) {
  //       usernames.push(`${i}`);
  //     }
  //     filter = { where: { username: { inq: usernames } } };
  //   }

  //   User.find(filter).then(users=>{
  //     return users.map(user=>user.id);
  //   }).then(ids=>{
  //     let thumbsups =
  //       lodash
  //         .chain(ids)
  //         .shuffle()
  //         .take(lodash.random(2, 10))
  //         .map(userId=>({itemId, userId}))
  //         .value();

  //     let total = thumbsups.length;
  //     let current = 1;

  //     setTimeout(()=> {
  //       Promise.resolve(thumbsups).each(thumbsup=>{
  //         return Promise.delay(lodash.random(2*60*1000, 10*60*1000)).then(()=>{
  //           return Thumbsup.findOrCreate({
  //             where: {
  //               itemId: thumbsup.itemId,
  //               userId: thumbsup.userId
  //             }
  //           }, thumbsup).then(()=>{
  //             job.log(thumbsup);
  //             job.progress(current ++, total)
  //           });
  //         });
  //       }).then(()=>done()).catch(console.log);
  //     }, FIVE_MINUTES);
  //   });

  // });

  DisableThumbsupRemote(Thumbsup);

  Thumbsup.observe('after save', function increaseThumbsups(ctx, next){
    next();

    const Activity = loopback.getModel('activity');
    const Message =  loopback.getModel('message');
    let instance = ctx.instance;

    process.nextTick(()=>{
      instance.activityId && increaseActivityThumbsups(instance);
      instance.itemId && increaseItemThumbsups(instance);
    })

    function increaseActivityThumbsups(thumbsup){
      return thumbsup.activity.getAsync().then(activity=>{
        return activity && activity.updateAttributes({
          thumbsups: activity.thumbsups + Math.floor(Math.random()*14)+1
        });
      }).catch(console.log);
    }

    function increaseItemThumbsups(thumbsup){
      return thumbsup.item.getAsync().then(item=>{
        return item && Thumbsup.count({itemId: item.id}).then(count=>{
          return item.updateAttributes({ thumbsups: count});
        })
      }).then(item=>{
        return item && Message.create({
          type: 'thumbsup',
          authorId: instance.userId,
          replyId: item.userId,
          itemId: item.id
        })
      }).catch(console.log);
    }
  });

  Thumbsup.observe('before delete', function decreaseThumbsups(ctx, next) {
    next();
    const Item = loopback.getModel('item');

    Item.findById(ctx.where.itemId).then(item=>{
      let thumbsups = item.thumbsups - 1 < 0 ? 0 : item.thumbsups - 1;
      return item.updateAttributes({thumbsups: thumbsups});
    }).catch(console.log);
  });

  Thumbsup.adminThumsup = function(userId, activityId, callback) {
    if(userId !== 10) {
      callback(Error('只有指定的用户可以调用！'));
    }

    const Activity = loopback.getModel('activity');
    Activity.findById(activityId).then(activity=>{
      activity.updateAttributes({thumbsups: activity.thumbsups + 1}).then(()=>{
        callback(null, {name: activity.text, thumbsups: activity.thumbsups});
      });
    });
  };

  Thumbsup.remoteMethod('adminThumsup',{
    accepts: [{
      arg: 'userId',
      type: 'number',
      required: true
    },{
      arg: 'activityId',
      type: 'number',
      required: true
    }],
    http: {verb: 'get', status: 200},
    description: '管理员点赞机',
    isStatic: true,
    returns: {type: "object", root: true}
  });
};
