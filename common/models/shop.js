var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Shop) {
  RemoteRouting(Shop, { only: [
    '@find',
    '@findById',
    '__get__products'
  ] });

  Shop.validatesInclusionOf('status', {in: ['online', 'offline']});
};
