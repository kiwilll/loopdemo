var RemoteRouting = require('loopback-remote-routing');
var bunyan = require('bunyan');

let logger = bunyan.createLogger({name: 'ZCM'});

module.exports = function(FigureBrand) {
  RemoteRouting(FigureBrand, {only: [
    '@find',
    '__get__figures'
  ]});

  /*
   * return a list brands that matches keyword, limit to 10
   *
   * @params {String} keyword
   * @params {Number} skip
   * @callback {Function} callback
   * @params {Error} err
   * @params {[FigureBrand]} brands
   */
  FigureBrand.search = (keyword, skip, callback)=>{
    let filter = {
      limit: 10,
      order: ['id ASC'],
      skip: skip || 0
    };

    if (keyword) {
      filter.where = { name: {regexp: `/${keyword}/i`} }
    }

    FigureBrand.find(filter).then(figures=>callback(null, figures)).catch(err=>{
      logger.error(err);
      return callback(err);
    });
  };

  FigureBrand.remoteMethod('search', {
    accepts: [ {
      arg: 'keyword',
      type: 'string',
      http: {source: 'query'},
      description: '搜索关键字'
    }, {
      arg: 'skip',
      type: 'number',
      http: {source: 'query'},
      description: '搜索偏移量'
    } ],
    http: {verb: 'get'},
    returns: {root: true, type: ['object']}
  });
};
