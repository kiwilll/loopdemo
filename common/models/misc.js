var DisableMiscRemote = require('../disables/misc.js');
var loopback = require('loopback');

module.exports = function(Misc) {
  DisableMiscRemote(Misc);

  Misc.afterRemote('findById', function isCollector(ctx, instance, next) {
    const Collection = loopback.getModel('collection');
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;

    if (userId) {
      Collection.count({miscId: instance.id, userId}).then(count=>{
        instance.isCollector = count ? true : false;
        next();
      }).catch(err=> {
        console.log(err);
        next();
      })
    } else {
      instance.isCollector = false;
      next();
    }
  });
};
