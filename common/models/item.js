var Promise = require('bluebird');
var co = require('co');
var loopback = require('loopback');
var DisableItemRemote = require('../disables/item.js');
var qiniu = require('qiniu');
var lodash = require('lodash');


// let options = {};

// if (process.env.NODE_ENV === 'staging') {
    // options.redis = {auth: 'Fanfan123'};
  // options.redis = {auth: '5eb280e28065c26571b8e876ab4e0e4a'}
// }

// if (process.env.NODE_ENV === 'production') {
//   options.redis = {
//     port: 6379,
//     host: '120.55.190.89',
//     auth: '5eb280e28065c26571b8e876ab4e0e4a'
//   }
// }

// let queue = kue.createQueue(options);

// let qiniuConfig;

// switch(process.env.NODE_ENV) {
//   case 'production':
//     qiniuConfig = require('../../server/config.production.json').qiniu;
//     break;
//   case 'staging' :
    qiniuConfig = require('../../server/config.staging.json').qiniu;
//     break;
//   default:
//     qiniuConfig = require('../../server/config.json').qiniu;
//     break;
// }

qiniu.conf.ACCESS_KEY = qiniuConfig.AK;
qiniu.conf.SECRET_KEY = qiniuConfig.SK;

module.exports = function(Item) {

  DisableItemRemote(Item);

  /*
   * REFACTOR: USE RECURSION
   *
   * 1. {a: 1, b: 2}
   * 2. {a:1, userId: 1}
   *
   * 3. {where: {a: 1, b: 2}, include: [], filter...}
   * 4. {where: {a: 1,userId: 2}, include: [], filter...}
   *
   * 5. {where: {or: [{a: 1,userId: 2}, {b: 1, c: 2}], include: [], filter...}
   * 6. {where: {and: [{a: 1,userId: 2}, {b: 1, c: 2}], include: [], filter...}
   */

  Item.observe('access', function addVisibleAdnDeletedScope(ctx, next){
    if (ctx.query.where) {
      if (ctx.query.where.or || ctx.query.where.and) {
        ['and', 'or'].forEach(operator=>{
          if (ctx.query.where[operator]) {
            ctx.query.where[operator] = ctx.query.where[operator].map(condition=>{
              condition.deleted = false;
              condition.visible = true;
              return condition;
            });
          }
        });
      } else {
        if((typeof ctx.query.where.visible) == 'undefined') {
          ctx.query.where.visible = true;
        }
        if((typeof ctx.query.where.deleted) == 'undefined') {
          ctx.query.where.deleted = false;
        }
        
      }
    } else {
      ctx.query.visible = true;
      ctx.query.deleted = false;
      let where = {};
      Object.keys(ctx.query).forEach(key=>{
        if (key != 'fields' && key != 'include' && key != 'order' && key != 'limit' && key != 'skip' && key != 'offset') {
          where[key] = ctx.query[key];
        }
      });
      ctx.query.where = where;
    }
    next();
  });

  Item.observe('access', function(ctx, next){
    let context = loopback.getCurrentContext();
    let currentUser = context && context.get('accessToken') && context.get('accessToken').userId;

    if (currentUser) {
      let originWhere = lodash.assign({}, ctx.query.where);
      let visibleWhere = lodash.assign({}, ctx.query.where);
      delete visibleWhere.visible;

      if (ctx.query.where) {
        if (ctx.query.where.or || ctx.query.where.and) {
          ['or', 'and'].forEach(operator=>{
            if (ctx.query.where[operator]) {
              ctx.query.where[operator] = ctx.query.where[operator].map(condition=>{
                if (operator === 'and')  delete condition.visible;
                if (condition.userId === currentUser) delete condition.visible;
                return condition;
              });
            }
          });
        } else {
          if (ctx.query.where.userId) {
            if (ctx.query.where.userId == currentUser) { ctx.query.where = visibleWhere; }
          } else {
            ctx.query.where = {
              or: [
                originWhere,
                lodash.assign({}, visibleWhere, {userId: currentUser})
              ]
            };
          }
        }
      } else {
        if (ctx.query.userId && ctx.query.userId === currentUser) {delete ctx.query.visible;}
      }
    } else {
        //do nothing;
    }

    next();
  })


  /*
   * if exists instance.user, then add followed to instance.user
   * {
   *  content: '',
   *  userId: '',
   *  user: {
   *    ...
   *  },
   *  followed: true/false
   * }
   */
  Item.afterRemote('findById', function followUser(ctx, instance, next){
    const Follow = loopback.getModel('follow');
    const loggedUserId = ctx.req.accessToken && ctx.req.accessToken.userId;
    if (!instance.__data.user) {
      return next();
    }

    if (loggedUserId) {
      Follow.count({
        followerId: loggedUserId,
        followeeId: instance.userId
      }).then(count=>{
        instance.followed = count ? true : false;
        next();
      }).catch(()=>next());
    } else {
      instance.followed = false;
      next();
    }
  });

  /*
   * if exists instance.thumbsup_users, then add followed to thumbsup_users
   * {
   *  content: '',
   *  userId: '',
   *  thumbsup_users: [
   *  {username: '', followed: true/false},
   *  {username: '', followed: true/false}
   *  ]
   * }
   */
  Item.afterRemote('findById', function followThumbsupUsers(ctx, instance, next){
    const Follow = loopback.getModel('follow');
    const loggedUserId = ctx.req.accessToken && ctx.req.accessToken.userId;

    if (!loggedUserId) {
      return Promise.resolve(instance.__data.thumbsup_users).map(user=>{
        user.followed = false;
        return user;
      }).then(()=>next()).catch(()=>next());
    }

    if (instance.__data.thumbsup_users && instance.__data.thumbsup_users.length) {
      Promise.resolve(instance.__data.thumbsup_users).map(user=>{
        return Follow.count({
          followerId: loggedUserId,
          followeeId: user.id
        }).then(count=>{
          user.followed = count ? true : false;
          return user;
        });
      }).then(()=> next()).catch(()=>next());
    } else {
      next();
    }
  });

  /*
   *
   * findById / find /findOne results will be modified by adding a
   * thumbsupOwner property to indicate whether a user gave
   * a thumbsup or not
   */
  Item.afterRemote('findById', (ctx, instance, next)=> {
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;
    const Thumbsup = loopback.getModel('thumbsup');

    if (instance) {
      if (userId) {
        Thumbsup.find({ where: { itemId: instance.id, userId } }).then(thumbsups=>{
          instance.thumbsupOwner = thumbsups.length ? true : false;
          next();
        });
      } else {
        instance.thumbsupOwner = false;
        next();
      }
    } else {
      next();
    }

    process.nextTick(()=> {
      instance && instance.updateAttributes({
        views: instance.views+ (Math.random() > 0.5 ? 2 : 3)
      }).catch(console.log);
    });
  });

  Item.publish = function(item, cb){
    if (!item.categoryId) {
      let error = new Error('categoryId is required');
      error.statusCode = 400;
    }

    if (!item.tags || !item.tags.length) {
      let error = new Error('tags are required');
      error.statusCode = 400;
      return cb(error);
    }

    item.tags = uniqueTags(trimTags(item.tags));

    function uniqueTags (tags) {
      let uniqueTags = {};
      tags.forEach(tag => { uniqueTags[tag] = 1; });
      return Object.keys(uniqueTags);
    }

    function trimTags(tags) {
      return tags.map(tag=>tag.trim());
    }

    if (!item.images || !item.images.length) {
      let error = new Error('images are required');
      error.statusCode = 400;
      return cb(error);
    }

    let [images, tags] = [item.images, item.tags];
    item.tagString = item.tags.join(',');
    delete item.images;
    delete item.tags;
    let Tag = loopback.getModel('tag');
    let TagItem = loopback.getModel('tagitem');
    let Category = loopback.getModel('category');
    let itemInstance;

    Item.create(item).then(item=>{
      itemInstance = item.toJSON();

      return Promise.props({
        tags: Promise.map(tags, tag=> Tag.findOrCreate({
          where: {
            name: tag
          }
        }, {name: tag}) ),
        images: item.images.create(images)
      });
    }).then(result=>{
      itemInstance.images = result.images;
      itemInstance.tags = result.tags.map(tag=>tag[0]);

      return Promise.map(result.tags, tag=>{
        return TagItem.create({itemId: itemInstance.id, tagId: tag[0].id});
      });
    }).then((tagItems)=> {
      cb(null, itemInstance);
      process.nextTick(()=>{
        Category.findById(item.categoryId).then(category=> {
          itemInstance.tags.forEach(tag=> category.tags.add(tag).catch(console.log));
        });
      });
    }).catch(cb);
  };

  Item.remoteMethod('publish', {
    accepts: {
      arg: 'data', type: 'object', required: true, http: { source: 'body' }
    },
    description: 'Publish an item with images and tags',
    returns: {
      type: 'object', root: true
    }
  });

  Item.afterRemote('publish', (ctx, instance, next)=>{
    co(function*() {
      const Daily = Item.app.models.daily;
      let result = yield Promise.promisify(Daily.do)(instance.userId, '发帖');
      instance.account = result;
      next();
      // queue.create('publish', {title: 'auto-thumbsup', itemId: instance.id}).save();
    }).catch(next);
  });

  Item.uptoken = function(callback) {
    let putPolicy = new qiniu.rs.PutPolicy(qiniuConfig.bucketname);
    let returnBody = {
      path: '$(key)',
      name: '$(fname)',
      width: '$(imageInfo.width)',
      height: '$(imageInfo.height)',
      type: '$(mimeType)'
    }
    putPolicy.returnBody = JSON.stringify(returnBody);
    callback(null, putPolicy.token());
  };

  Item.remoteMethod('uptoken', {
    description: 'Return qiniu upload token',
    http: {verb: 'get'},
    returns: {
      arg: 'token', type: 'object'
    }
  });

  Item.search = function(filter, keyword, callback) {
    filter = filter || {};

    delete filter.where;

    filter.include = [
      'commenters',
      {relation: 'images', scope: {order: 'order ASC'}}
    ];

    let query = {regexp: `/${keyword}/i`};

    keyword && (filter.where = {
      or: [
        {content: query, deleted: false, visible: true},
        {tagString: query, deleted: false, visible: true}
      ]
    });

    Item.find(filter).then(items=> callback(null, items)).catch(callback);
  };

  Item.remoteMethod('search', {
    description: 'Return items that matche filter',
    accepts: [ {
      arg: 'filter',
      type: 'object',
      http: {source: 'query'},
      description: 'Filter defining fields, order, offset'
    }, {
      arg: 'keyword',
      type: 'string',
      http: {source: 'query'}
    } ],
    http: {verb: 'get'},
    returns: { root: true, type: 'array' }
  });

  Item.prototype.boom = function(callback) {
    callback(null, this);
  };

  Item.beforeRemote('prototype.boom', (ctx, instance, next)=>{
    let Collection = loopback.getModel('collection');
    let Thumbsup = loopback.getModel('thumbsup');
    let itemInstance = ctx.instance;

    if (ctx.req.accessToken.userId == itemInstance.userId) {
      itemInstance.updateAttributes({deleted: true}).then(()=> {
        Collection.destroyAll({ itemId: itemInstance.id }).catch(console.log);
        Thumbsup.destroyAll({ itemId: itemInstance.id }).catch(console.log);
        next();
      }).catch(next);
    } else {
      let error = new Error('Authorization Error');
      error.statusCode = 401;
      next(error);
    }
  })

  Item.remoteMethod('boom', {
    accepts: [],
    http: {verb: 'put', status: 204},
    description: 'Remove an item',
    isStatic: false,
    returns: { root: true, type: 'object' }
  });

  Item.prototype.removeComment = function(commentId, callback) {
    const Comment = loopback.getModel('comment');
    Comment.destroyById(commentId).then(()=>callback(null)).catch(callback);
  }

  Item.remoteMethod('removeComment', {
    accepts: [{
      arg: 'commentId',
      type: 'number',
      require: true,
      http: {source: 'path'}
    }],
    isStatic: false,
    http: {verb: 'delete', path: '/comments/:commentId'},
    description: 'Remove comment under an item',
    returns: {root: true, type: 'object'}
  })
};
