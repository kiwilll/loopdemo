var DisableCommentRemote = require('../disables/comment.js');
var loopback = require('loopback');

module.exports = function(Comment) {
  let Message = loopback.getModel('message');

  DisableCommentRemote(Comment);

  /*
   * a simple way to prevent users from commenting when user is in block list;
   */
  Comment.beforeRemote('create', function disableComment(ctx, instance, next){
    const blockedUsers = [4203, 4218];
    const userId = ctx.args && ctx.args.data && ctx.args.data.userId;
    if (userId === 4203 || userId === 4218) {
      let error = new Error('FORBIDDEN');
      error.statusCode = 403;
      return next(error);
    } else {
      next();
    };
  });

  Comment.afterRemote('create', function updateItemUpdatedAt(ctx, instance, next){
    next();
    instance && instance.item.getAsync().then(item=>{
      return item.updateAttributes({
        updated_at: instance.updated_at
      }).catch(console.log)
    })
  });

  Comment.afterRemote('create', function notify(ctx, instance, next){
    next();

    process.nextTick(()=>{
      if (instance.replyId) {
        let message = {
          type: 'reply',
          replyId: instance.replyId,
          authorId: instance.userId
        };

        if (instance.itemId) {
          message.itemId = instance.itemId;
        }

        if (instance.activityId) {
          message.type = 'reply_activity';
          message.activityId = instance.activityId;
        }

        if (instance.figureId) {
          message.type = 'replay_figure';
          message.figureId = instance.figureId;
        }

        Message.create(message).catch(console.log);
      } else {
        instance.itemId && instance.item.getAsync().then(item=>{
          return Message.create({
            type: 'comment',
            authorId: instance.userId,
            itemId: instance.itemId,
            replyId: item.userId
          });
        }).catch(console.log);
      }
    });

  });

  Comment.beforeRemote('count', (ctx, instance, next) => {
    if(!ctx.req.query.where) return next();

    let where;

    try{
      where = (typeof ctx.req.query.where === 'object') ? ctx.req.query.where : JSON.parse(ctx.req.query.where);
    }catch(e){
      console.log(e);
      return next(e);
    }

    if(!where.instModel || !where.instId) return next();

    if(where.instModel !== "item" && where.instModel !== "activity") return next();

    where[where.instModel+'Id'] = where.instId;

    delete where.instModel;
    delete where.instId;

    ctx.req.query.where = where;
    ctx.args.where = where;

    next();
  });

  /**
   * if comment has nested comments, remove them
   */
  Comment.beforeRemote('deleteById', function delNestedComments(ctx, instance, next) {
    Comment.destroyAll({commentId: ctx.args.id}, (err)=>{
      return err ? next(err) : next();
    })
  });

  Comment.deleteByIdForAdmin = function(id, callback) {
    Comment.destroyAll({commentId: id}).then(()=>{
      return Comment.destroyById(id);
    }).then(()=>{
      callback(null, {result: true});
    }).catch(callback);
  };

  Comment.remoteMethod('deleteByIdForAdmin', {
    http: {path: '/admin/:id', verb: 'delete', status: 200},
    description: '管理员删除评论',
    accepts: {arg: 'id', type: 'number', http: { source: 'path'}, required: true},
    returns: { root: true, 'type': 'boolean'}
  });
};
