var DisableMessageRemote = require('../disables/message.js');
var loopback = require('loopback');
// var Promise, {coroutine as co} = require('bluebird'); old
var Promise = require('bluebird');

module.exports = function(Message) {

  DisableMessageRemote(Message);

  /**
   *  when creating a notification message, retrive extra info
   *  extra.image: image associted with the message,
   *  extra.authorInfo: the info about the user who initiate the notification
   */
  Message.observe('before save', (ctx, next)=>{
    const app       = Message.app;
    const User      = app.models.user;
    const Image     = app.models.image;
    const Activity  = app.models.activity;
    const Product   = app.models.product;

    // skip deleteAll, updateAll and updateOrCreate operations
    if (!ctx.instance) {
      return next();
    }

    getExtraInfo(ctx.instance).then(extra => {
      ctx.instance.extra = extra;
      next();
    }).catch((err) => {
      console.log(err);
      next();
    })

    function getExtraInfo(instance) {
      return co(function*() {
        let image =  yield getImage(ctx.instance);
        let authorInfo =  yield getAuthorInfo(ctx.instance.authorId);
        return { image, authorInfo };
      })();
    };

    function getAuthorInfo(authorId) {
      return User.findById(authorId, {
        fields: {
          id: 1,
          nickname: 1,
          headimg: 1
        }
      });
    }

    function getImage (data = {}) {
      if (!needToFetchImage(data)) {
        return Promise.resolve(null);
      }

      if (data.activityId) {
        return Activity.findById(data.activityId, {
          fields: {
            thumbnail: 1,
            path: 1
          }
        });
      }

      return Image.findOne({
        where: {
          imageableId: data.instId || data.itemId,
          imageableType: data.instModel || 'item'
        },
        fields: {path: 1},
        order: 'order ASC'
      });
    }

    function needToFetchImage(instance) {
      return (instance.itemId ||
              instance.activityId ||
              instance.figureId ||
              (instance.instId && instance.instModel));
    };
  });

  Message.beforeRemote('deleteById', (ctx, instance, next)=> {
    Message.findById(ctx.req.params.id).then(message=>{
      if (ctx.req.accessToken.userId == message.replyId) {
        next();
      } else {
        let error = new Error('Authorization Error');
        error.statusCode = 401;
        next(error);
      }
    });
  });

  Message.prototype.markread = function(callback) {
    callback(null, this);
  };

  Message.beforeRemote('prototype.markread', (ctx, instance, next)=> {
    if (ctx.req.accessToken.userId == instance.replyId) {
      instance.updateAttributes({read: true}).then(()=>{
        next();
      }).catch(next);
    } else {
      let error = new Error('Authorization Error');
      error.statusCode = 401;
      next(error);
    }
  });

  Message.remoteMethod('markread', {
    accepts: [],
    http: {verb: 'put'},
    isStatic: false,
    description: 'mark a message read',
    returns: {type: 'object', root: true}
  });

  Message.markread_all = function(callback) {
    callback(null);
  };

  Message.beforeRemote('markread_all', (ctx, instance, next)=> {
    Message.updateAll({
      replyId: ctx.req.accessToken.userId,
      read: false
    }, {read: true}, (err, results)=> {
      if (err) return next(err);
      next();
    })
  });

  Message.remoteMethod('markread_all', {
    accepts: [],
    http: {verb: 'put', status: 204},
    description: 'Mark all messages to read'
  });

  Message.mine = function(req, filter, callback) {
    filter = filter || {};
    filter.where = filter.where || {};
    filter.order = 'created_at DESC';
    delete filter.include;
    filter.where.replyId = req.accessToken.userId;


    Message.find(filter).then((messages)=>{
      callback(null, messages);
    }).catch(callback);
  };

  Message.remoteMethod('mine', {
    accepts: [{
      arg: 'req', type: 'object', http: {source: 'req'}
    }, {
      arg: 'filter', type: 'object', required: false
    }],
    http: {verb: 'get'},
    description: 'Get messages that belongs to a user',
    returns: {
      type: 'object', root: true
    }
  });
};
