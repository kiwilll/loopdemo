var RemoteRouting = require('loopback-remote-routing');
var co = require('co');
var loopback = require('loopback');

module.exports = function(Album) {
  RemoteRouting(Album, {only: [
    '@find'
  ]});
}
