var loopback = require('loopback');
var Promise = require('bluebird');
var co = require('co');

module.exports = function(Daily) {
  const Account = loopback.getModel('account');

  Daily.attendance = function(userId, callback) {
    const timemark = new Date();
    const unix_day = Math.floor(timemark.valueOf() / 1000 / 3600 / 24);
    Daily.findOne({
      where: {
        userId: userId,
        type: '签到',
        'unix-day': unix_day
      }
    }).then(today=>{
      if(!today){ //今日还未签到
        let attendance = {
          userId: userId,
          type: '签到',
          timemark: timemark,
          'unix-day': unix_day,
          year: timemark.getFullYear(),
          month: timemark.getMonth() + 1,
          day: timemark.getDate()
        };

        Daily.findOne({
          where: {
            userId: userId,
            type: '签到',
            'unix-day':
            unix_day - 1
          }
        }).then(yestoday=>{
          if(!yestoday) { //非连续签到
            attendance.last = 1;
            attendance.point = Daily['签到'].point;
            attendance.exp = Daily['签到'].point;
          } else { //连续签到
            attendance.last = yestoday.last + 1;
            if(attendance.last >= 30) {
              attendance.point = Daily['连续签到-30'].point;
              attendance.exp = Daily['连续签到-30'].point;
            } else if(attendance.last >= 7) {
              attendance.point = Daily['连续签到-7'].point;
              attendance.exp = Daily['连续签到-7'].point;
            } else {
              attendance.point = Daily['连续签到'].point;
              attendance.exp = Daily['连续签到'].point;
            }
          }

          Daily.create(attendance)
          .then((today)=>{
            Account.findOne({
              where: {
                userId: userId
              }
            }).then(account=>{
              let text = '';
              if(today.last >= 30) {
                text = '';
              } else if(today.last >= 7) {
                text = '再签到' + (30 - today.last) + '天可以获得更多草莓！';
              } else if(today.last > 1) {
                text = '再签到' + (7 - today.last) + '天可以获得更多草莓！';
              } else if(today.last == 1) {
                text = '明天签到可以获得更多草莓！';
              }
              account.balance({
                point: today.point
              }, '日常:签到',()=>{
                return callback(null, {
                  point: today.point,
                  totalPoint: account.point,
                  last: today.last,
                  text: text
                });
              });
            }).catch(callback);
          }).catch(callback);
        }).catch(callback);

      } else { //今日已签到
        return callback(new Error('今日已签到'));
      }
    }).catch(callback);
  }

  Daily.getAttendances = function(userId, year, month, callback) {
    Daily.find({
      where: {
        userId: userId,
        type: '签到',
        year: year,
        month: month
      }
    }).then(attendances=>{
      return attendances.map((attendance)=>{
        return attendance.day
      });
    }).then(days=>{
      const unix_day = Math.floor(Date.now().valueOf() / 1000 / 3600 / 24);
      Daily.findOne({
        where: {
          userId: userId,
          type: '签到',
          'unix-day': unix_day
        }
      }).then(today=>{
        Account.findOne({
          where: {
            userId: userId
          }
        }).then(account=>{
          if(!today) {
            return callback(null, {days: days, last: 0, text: '', totalPoint: account.point});
          } else {
            let text = '';
            if(today.last >= 30) {
              text = '';
            } else if(today.last >= 7) {
              text = '再签到' + (30 - today.last) + '天可以获得更多草莓！';
            } else if(today.last > 1) {
              text = '再签到' + (7 - today.last) + '天可以获得更多草莓！';
            } else if(today.last == 1) {
              text = '明天签到可以获得更多草莓！';
            }
            return callback(null, {days: days, last: today.last, text: text, totalPoint: account.point});
          }
        }).catch(callback);
      }).catch(callback);
    }).catch(callback);
  }

  Daily['签到'] = {point: 5};
  Daily['连续签到'] = {point: 7};
  Daily['连续签到-7'] = {point: 10};
  Daily['连续签到-30'] = {point: 20};
  Daily['发帖'] = {point: 10, time: 2};
  Daily['回复'] = {point: 4, time: 5};
  Daily['点赞'] = {point: 2, time: 10};
  Daily['分享'] = {point: 20, time: 1};
  Daily['购买'] = {point: 20, time: 5};

  Daily.do = function(userId, type, callback) {
    const timemark = new Date();
    const unix_day = Math.floor(timemark.valueOf() / 1000 / 3600 / 24);
    Daily.count({
      userId: userId,
      type: type,
      'unix-day': unix_day
    }).then(count=>{
      if(count < Daily[type].time){ //次数未满
        let daily = {
          userId: userId,
          type: type,
          timemark: timemark,
          'unix-day': unix_day,
          year: timemark.getFullYear(),
          month: timemark.getMonth() + 1,
          day: timemark.getDate(),
          point: Daily[type].point,
          exp: Daily[type].point
        };
        Daily.create(daily)
        .then((daily)=>{
          Account.findOne({where:{userId: userId}})
          .then(account=>{
            account.balance({
              point: daily.point
            },'日常:' + type);

            if(callback) {
              return callback(null, {
                success: true,
                totalPoint: account.point,
                point: daily.point
              });
            }
          }).catch(callback);
        });
      } else {
        if(callback) {
          return callback(null, {success: false});
        }
      }
    }).catch(callback);
  }

  Daily.get = (userId, callback)=>{
    co(function*() {
      let unix_day = Math.floor(Date.now().valueOf() / 1000 / 3600 / 24);
      let dailys = [];
      let types = ['发帖', '回复', '分享', '点赞'];
      for(let i = 0; i < types.length; i++) {
        let count = yield Daily.count({
          userId: userId,
          type: types[i],
          'unix-day': unix_day
        });
        dailys.push({type: types[i], count: count, total: Daily[types[i]].time});
      }
      return callback(null, dailys);
    }).catch(callback);
  }
}