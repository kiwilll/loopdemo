var DisableCollectionRemote = require('../disables/collection.js');
var loopback = require('loopback');

module.exports = function(Collection) {

  DisableCollectionRemote(Collection);

  Collection.observe('after save', (ctx, next)=> {
    next();

    let Message = loopback.getModel('message');
    let instance = ctx.instance;

    instance && process.nextTick(()=>{
        instance.item.getAsync().then(item => {
          return item && Message.create({
            type: 'collect',
            authorId: instance.userId,
            itemId: instance.itemId,
            replyId:  item.userId
          });
        }).catch(console.log);
      });
  });
};
