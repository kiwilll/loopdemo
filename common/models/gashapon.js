var loopback = require('loopback');
var Promise = require('bluebird');
var co = require('co');
var Chance = require('chance');
var RemoteRouting = require('loopback-remote-routing');
// var redis = require('redis');
// Promise.promisifyAll(redis.RedisClient.prototype);
var request = require('request');

module.exports = function(Gashapon) {
  RemoteRouting(Gashapon, {only: [
    '@find',
    'roll',
    'exchange'
  ]});

  // let mobiledataConfig = null;

  // switch(process.env.NODE_ENV) {
  //   case 'production':
  //     mobiledataConfig = require('../../server/config.production.json').submail.mobiledata;
  //   break;
  //   case 'staging' :
      // mobiledataConfig = require('../../server/config.staging.json').submail.mobiledata;
  //   break;
  //   default:
  //     mobiledataConfig = require('../../server/config.json').submail.mobiledata;
  //   break;
  // }

  /* 
   * 抽奖逻辑
   */
  // Gashapon.redis_client = null;
  // let client_name = process.env.NODE_ENV + '-gashapon-';
  
  // let getAllGashapons = ()=>{
  //   let getAll = ()=>{
  //     let rollFrom = function(min, max, num, excludes) {
  //       const chance = new Chance();
  //       const result = [];
  //       let loopcount = 0;
  //       while(result.length < num && loopcount < 1000) {
  //         const n = min + chance.integer({min: min, max: Math.max(min, max - 1)});
  //         if(result.indexOf(n) === -1 && excludes.indexOf(n) === -1) {
  //           result.push(n);
  //           excludes.push(n);
  //         }
  //       }
  //       return result;
  //     }

      // co(function*(){
      //   let client = Gashapon.redis_client;

      //   let gashapons = yield Gashapon.find();
      //   yield gashapons.map(function* (gashapon) {
      //     let exists = yield client.existsAsync(client_name + 'counter-' + gashapon.id);
      //     if(!exists) {
      //       yield client.setAsync(client_name + 'counter-' + gashapon.id, -1);
      //     }

      //     exists = yield client.existsAsync(client_name + gashapon.id);
      //     if(exists) {
      //       return;
      //     }
      //     const Award = loopback.getModel('award');
      //     let awards = yield Award.find({where: {
      //       and: [{
      //         id: {inq: gashapon.awards}
      //       },{
      //         period: '非常驻'
      //       },{
      //         or: [{
      //           left: null
      //         }, {
      //           left: {gt: 0}
      //         }]
      //       }]
      //     }});

      //     const excludes = [];
      //     for(let i = 0; i < awards.length; i++) {
      //       let gashapon_i = [];
      //       let floor = 0;
      //       for(let j = 0; j < awards[i].left.length; j++) {
      //         gashapon_i = gashapon_i.concat(rollFrom(floor, awards[i].left[j].limit, awards[i].left[j].num, excludes));
      //         floor = awards[i].left[j].limit;
      //       }
      //       console.log('gashapon' + gashapon.id +': ' + gashapon_i);
      //       yield client.hmsetAsync(client_name + gashapon.id,
      //         'award-' + awards[i].id, JSON.stringify(gashapon_i)
      //       );
      //     }
      //   });
      // }).catch(console.log)
    // }

    // if(!Gashapon.redis_client) {
      // Gashapon.redis_client = redis.createClient('6379', '127.0.0.1',{password: 'Fanfan123'});
      // redis.createClient('6379', '68464d55ad114e47.redis.rds.aliyuncs.com', {password: 'Fanfan123'});
      // Gashapon.redis_client.on('ready', function() {
      //   getAll();
      // });
    // } else {
    //   getAll();
    // }
  // };
  // getAllGashapons();
  
  Gashapon.award = function(g, callback) {
    // co(function* () {
    //   const Award = loopback.getModel('award');
    //   const chance = new Chance();
    //   let now = Date.now();
      
    //   let awarded_currenttime = false;//中本期奖（大奖）
    //   let awards_currenttime = yield Award.find({where: {
    //     and: [{
    //       id: {inq: g.awards}
    //     },{
    //       period: '非常驻'
    //     },{
    //       or: [{
    //         left: null
    //       }, {
    //         left: {gt: 0}
    //       }]
    //     }]
    //   }});

    //   let awards_alltime = yield Award.find({where: {
    //     and: [{
    //       id: {inq: g.awards}
    //     },{
    //       period: '常驻'
    //     },{
    //       or: [{
    //         left: null
    //       }, {
    //         left: {gt: 0}
    //       }]
    //     }]
    //   }});

    //   let awardIndex = null;

    //   let client = Gashapon.redis_client;
    //   let gashapon = yield client.hgetallAsync(client_name + g.id);
    //   let counter = yield client.incrAsync(client_name + 'counter-' + g.id);

    //   for (let i = 0; i < awards_currenttime.length; i++) {

    //     let award_counts = JSON.parse(gashapon['award-' + awards_currenttime[i].id]);
    //     if(award_counts.indexOf(counter) != -1) {
    //       awardIndex = i;
    //       awarded_currenttime = true;
    //       return callback(null, {awarded: true,  award: awards_currenttime[awardIndex]});
    //     }
    //   }


    //   let awarded_alltime = chance.floating({min: 0, max: 100}) < 40 && !awarded_currenttime;//中常驻奖品
    //   if(awarded_alltime) {
    //     let weight_total = 0;
    //     awards_alltime.map(award=>{
    //       weight_total += award.chance;
    //     });
    //     //至此weight_total 为总权重
    //     //令weight = [0, weight_total)
    //     let weight = chance.natural({min: 1, max: weight_total}) - 1;
    //     for(let i = 0; i < awards_alltime.length; i++) {
    //       let award = awards_alltime[i];
    //       if(weight < award.chance) { //奖品为award
    //         if(award.left) {
    //           award.updateAttribute('left', award.left - 1).catch(callback);
    //         }
    //         awardIndex = i;
    //         break;
    //       } else {
    //         weight -= award.chance;
    //       }
    //     };
    //     awards_alltime = awards_alltime.map(award=>{
    //       return {
    //         name: award.name,
    //         avatar: award.avatar,
    //         id: award.id,
    //         type: award.type,
    //         value: award.value
    //       }
    //     });
    //     return callback(null, {awarded: true, award: awards_alltime[awardIndex]});
    //   } else {
    //     return callback(null, {awarded: false});
    //   }
    // }).catch(callback);
  }

  /* 
   * 消耗抽奖券或积分，进行一次抽奖，并返回结果
   */
  Gashapon.prototype.roll = function (userId, callback) {
    // let self = this;
    // co(function*() {
    //   let start = new Date(self.time_start).valueOf();
    //   let stop = new Date(self.time_stop).valueOf();
    //   let end = new Date(self.time_end).valueOf();
    //   let now = Date.now().valueOf();
    //   if(now < start) {
    //     return callback(new Error('活动尚未开始！'));
    //   }
    //   if(now > stop) {
    //     return callback(new Error('抽奖已经结束！'));
    //   }

    //   const Account = loopback.getModel('account');
    //   let account = yield Account.findOne({
    //     where: {
    //       userId: userId
    //     }
    //   });

    //   let balance = {};
    //   if(account.gashapon > 0) {
    //     balance.gashapon = -1;
    //   } else {
    //     balance.point = -20;
    //   }

    //   Promise.promisifyAll(account);
    //   let result = yield account.balanceAsync(balance, '扭蛋消耗');

    //   let award_result = yield Promise.promisify(Gashapon.award)(self);
    //   result.awarded = award_result.awarded;

    //   const GashaponRecord = loopback.getModel('gashaponRecord');
    //   let gashapon_record = yield GashaponRecord.findOrCreate({
    //     where: {
    //       userId: userId,
    //       gashaponId: self.id
    //     }
    //   },{
    //     userId: userId,
    //     gashaponId: self.id
    //   });
    //   gashapon_record = gashapon_record[0];

    //   if(award_result.awarded) {
    //     award_result = award_result.award;
    //     result.award = award_result;
    //     if(award_result.type == '积分') {
    //       yield account.balanceAsync({point: parseInt(award_result.value)}, '扭蛋奖励');
    //     } else {
    //       let update = {
    //         lastAwardId: award_result.id
    //       };
    //       if(!gashapon_record.currentAwardId) {
    //         update.currentAwardId = award_result.id;
    //         delete update.lastAwardId;
    //       }

    //       yield gashapon_record.updateAttributes(update);
    //     }
    //   } else {
    //     yield gashapon_record.updateAttribute('lastAwardId', null);
    //   }

    //   return callback(null, result);
    // }).catch(callback);
  }

  Gashapon.beforeRemote('prototype.roll', function(ctx, instance, next){
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;
    if(ctx.req.body.userId != userId) {
      return next(new Error('只能用自己的ID抽奖'));
    }
    next();
  });

  Gashapon.remoteMethod('roll', {
    accepts: [{
      arg: 'userId',
      type: 'number',
      required: true
    }],
    http: {verb: 'post', status: 200},
    description: '扭蛋抽奖',
    isStatic: false,
    returns: {type: "object", root: true}
  });

  Gashapon.beforeRemote('confirm', function(ctx, instance, next){
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;
    if(ctx.req.body.userId != userId) {
      return next(new Error('只能领取自己ID的奖品'));
    }
    next();
  });

  Gashapon.prototype.confirm = function(userId, callback) {
    let self = this;
    co(function*() {
      const GashaponRecord = loopback.getModel('gashaponRecord');
      let gashapon_record = yield GashaponRecord.findOne({
        where: {
          gashaponId: self.id,
          userId: userId
        }
      });

      if(gashapon_record.lastAwardId) {
        yield gashapon_record.updateAttributes({
          currentAwardId: gashapon_record.lastAwardId,
          lastAwardId: null
        });
      }
      let award = yield Promise.promisify(gashapon_record.award_current)();

      return callback(null, {success: true, award: award});
    }).catch(callback);
  };

  Gashapon.remoteMethod('confirm', {
    accepts: [{
      arg: 'userId',
      type: 'number'
    }],
    http: {verb: 'post', status: 200},
    description: '确认领奖',
    isStatic: false,
    returns: {type: "object", root: true}
  });

  Gashapon.beforeRemote('cancle', function(ctx, instance, next){
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;
    if(ctx.req.body.userId != userId) {
      return next(new Error('只能放弃自己ID抽到的奖品'));
    }
    next();
  });

  Gashapon.prototype.cancle = function(userId, callback) {
    var self = this;
    co(function*() {
      const GashaponRecord = loopback.getModel('gashaponRecord');
      console.log({
          gashaponId: self.id,
          userId: userId
        })
      let gashapon_record = yield GashaponRecord.findOne({
        where: {
          gashaponId: self.id,
          userId: userId
        }
      });

      yield gashapon_record.updateAttribute('lastAwardId', null);

      return callback(null, {success: true});
    }).catch(callback);
  };

  Gashapon.remoteMethod('cancle', {
    accepts: [{
      arg: 'userId',
      type: 'number',
      required: true
    }],
    http: {verb: 'post', status: 200},
    description: '放弃上次抽中奖品',
    isStatic: false,
    returns: {type: "object", root: true}
  });

  Gashapon.beforeRemote('query', function(ctx, instance, next){
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;
    if(ctx.req.body.userId != userId) {
      return next(new Error('只能查询自己ID抽到的奖品'));
    }
    next();
  });

  Gashapon.prototype.query = function(userId, callback) {
    var self = this;
    co(function*() {
      const GashaponRecord = loopback.getModel('gashaponRecord');
      let gashapon_record = yield GashaponRecord.findOne({
        where: {
          gashaponId: self.id,
          userId: userId
        }
      });

      if(gashapon_record) {
        let current = yield Promise.promisify(gashapon_record.award_current)();
        let last = yield Promise.promisify(gashapon_record.award_last)();

        return callback(null, {
          success: true,
          current: current || null,
          last: last || null
        });
      } else {
        return callback(null, {
          success: true,
          current: null,
          last: null
        });
      }
      
    }).catch(callback);
  };

  Gashapon.remoteMethod('query', {
    accepts: [{
      arg: 'userId',
      type: 'number',
      required: true
    }],
    http: {verb: 'post', status: 200},
    description: '查询当前持有的奖品',
    isStatic: false,
    returns: {type: "object", root: true}
  });

  Gashapon.prototype.award = function(callback) {
    var self = this;
    co(function*() {
      const Award = loopback.getModel('award');
      let awards = yield Award.find({
        where: {
          id: {inq: self.awards}
        },
        order: 'order ASC'
      });
      return callback(null, awards);
    }).catch(callback);
  };

  Gashapon.remoteMethod('award', {
    http: {verb: 'get', status: 200},
    description: '查询某次扭蛋活动的全部奖品',
    isStatic: false,
    returns: {type: "object", root: true}
  });


  Gashapon.beforeRemote('exchange', function(ctx, instance, next){
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;
    
    if(ctx.req.body.userId != userId) {
      return next(new Error('只能兑换自己的奖品！'));
    }
    next();
  });

  Gashapon.prototype.exchange = function(userId, callback) {
    var self = this;
    co(function*() {
      let start = new Date(self.time_start).valueOf();
      let stop = new Date(self.time_stop).valueOf();
      let end = new Date(self.time_end).valueOf();
      let now = Date.now().valueOf();
      if(now < start) {
        return callback(new Error('活动尚未开始！'));
      }
      if(now > end) {
        return callback(new Error('活动已经结束！'));
      }

      let GashaponRecord = loopback.getModel('gashaponRecord');
      let gashapon_record = yield GashaponRecord.findOne({
        where: {
          gashaponId: self.id,
          userId: userId
        }
      });
      if(!gashapon_record) {
        return callback(new Error('没有待领取的奖品！'));
      } else if(gashapon_record.state == '已领') {
        return callback(new Error('已经领取了本次活动的奖品！'));
      }
      
      let awardId = gashapon_record.currentAwardId;
      if(awardId || awardId == 0) {
        const Award = loopback.getModel('award');
        let award = yield Award.findById(awardId);

        const User = loopback.getModel('user');
        let user = yield User.findById(userId);
        if(!user.mobile) {
          return callback(new Error('尚未填写领奖手机号！'));
        }
        if(award.type == '流量') {
          let url = 'https://api.submail.cn/mobiledata/charge.json';
          let post = Promise.promisify(request.post);
          let result = yield post(url, {form: {
            appid: mobiledataConfig.appid,
            to: user.mobile,
            cm: award.value.cm,
            cu: award.value.cu,
            ct: award.value.ct,
            signature: mobiledataConfig.appkey
          }});

          if(result[0].body.status == 'error') {
            return callback(new Error(result[0].body.msg));
          }

          yield gashapon_record.updateAttribute('state', '已领');
        } else if(award.type == '实体奖品'){
          if(!user.address || !user.addressee) {
            return callback(new Error('请填写收件地址、收件人！'));
          }
          yield gashapon_record.updateAttribute('state', '已领');
        }

        return callback(null, award);
      } else {
        return callback(new Error('并没有持有任何奖品！'));
      }
      
    }).catch(callback);
  }

  Gashapon.remoteMethod('exchange', {
    accepts: [{
      arg: 'userId',
      type: 'number',
      required: true
    }],
    http: {verb: 'post', status: 200},
    description: '兑换本期奖品',
    isStatic: false,
    returns: {type: "object", root: true}
  });

  Gashapon.getCurrent = (callback)=>{
    co(function*() {
      let now = new Date();
      let current = yield Gashapon.findOne({
        where: {
          and: [{
            time_start: {lt: now}
          },{
            time_end: {gt: now}
          }]
        },
        order: 'time_stop DESC'
      });
      current = current.toJSON();
      current.current_time = now;
      return callback(null, current);
    }).catch(callback);
  }

  Gashapon.remoteMethod('getCurrent', {
    http: {verb: 'get', status: 200},
    description: '获得本期活动',
    isStatic: true,
    returns: {type: "object", root: true}
  });

  Gashapon.afterRemote('find', (ctx, instance, next)=> {
    co(function*() {
      let now = new Date();
      instance.map((gashapon)=>{
        gashapon.current_time = now;
      });

      next();
    }).catch(next);
  });

  Gashapon.getRule = (callback)=>{
    return callback(null, {
      text: '1.参加扭蛋活动需要消耗20个草莓，草莓可通过发布内容和点赞等等获得。\n'
          + '2.通过分享扭蛋活动到各个平台可以获得更多草莓。\n'
          + '3.扭蛋活动期间可以扭任意次数, 直到扭到自己满意的奖品, 并确定 (活动结束前只能确定一个奖品, 最新确定的奖品将会覆盖上一个奖品)\n'
          + '4.抽到且确定的奖品, 将进入我的奖池, 即最终可以获得的奖品\n'
          + '5.活动结束后进入领奖时间, 请进入扭蛋活动列表领取相应的奖品 (过期将无法领取奖品, 并且请务必填写联系地址, 没有填写的将视作放弃领奖资格)\n'});
  }

  Gashapon.remoteMethod('getRule', {
    http: {verb: 'get', status: 200},
    description: '获得扭蛋活动规则',
    isStatic: true,
    returns: {type: "object", root: true}
  });
}