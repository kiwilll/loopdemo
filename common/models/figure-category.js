var RemoteRouting = require('loopback-remote-routing');

module.exports = function(FigureCategory) {
  RemoteRouting(FigureCategory, { only: [
    '@find',
    '__get__figures',
    '__get__brands',
  ]});
};
