var loopback = require('loopback');
var Promise = require('bluebird');
var RemoteRouting = require('loopback-remote-routing');
var LV = require('../../server/libs/lv-calc.js');

module.exports = function(Account) {
  RemoteRouting(Account,{only: [
    'createAll'
  ]});

  let User = loopback.getModel('user');
  Account.createAll = function(callback) {
    User.find().then(users=>{
      let created_count = 0;
      Promise.each(users, (user, index, length)=>{
        Account.findOrCreate({
          userId: user.id
        },{
          userId: user.id,
          point: 100
        }).then((account, created)=>{
          if(created) {
            created_count++;
          }
          //console.log(index + '/' + length);
        });
      }).then(()=>{
        callback(null, {result: 'done', created: created_count});
      }).catch(callback);
    }).catch(callback);
  }

  Account.remoteMethod('createAll', {
    http: {verb: 'get'},
    description: '创建所有用户的账户',
    isStatic: true,
    returns: {type: "object", root: true}
  });

  Account.prototype.balance = function(earning, reason, callback) {
    let record = {
      current_point: this.point,
      current_exp: this.exp,
      current_gashapon: this.gashapon
    };

    let update = {};
    if(earning.point) {
      if(earning.point > 0) {
        record.point = earning.point;
        update.point = record.current_point + record.point;

        record.exp = earning.point;
        update.exp = record.current_exp + record.exp;
      } else { // earning.point < 0
        if(this.point < Math.abs(earning.point)) {
          callback(new Error('账户积分不足！需要:' + earning.point + '积分，剩余:' + this.point + '积分'), this);
        } else { // this.point >= Math.abs(earning.point)
          record.point = earning.point;
          update.point = record.current_point + record.point;
        }
      }
    }

    if(earning.gashapon) {
      if(earning.gashapon > 0) {
        record.gashapon = earning.gashapon;
        update.gashapon = record.current_gashapon + record.gashapon;
      } else { // earning.gashapon < 0
        if(this.gashapon < Math.abs(earning.gashapon)) {
          callback(new Error('抽奖次数不足！需要:' + earning.gashapon + '次，剩余:' + this.gashapon + '次'), this);
        } else { // this.gashapon >= Math.abs(earning.gashapon)
          record.gashapon = earning.gashapon;
          update.gashapon = record.current_gashapon + record.gashapon;
        }
      }
    }

    record.reason = reason;
    record.timemark = new Date();
    
    this.records.create(record).catch(callback);

    this.updateAttributes(update).then(account=>{
      let result = {};
      result.userId = account.userId;
      result.point = record.point || 0;
      result.gashapon = record.gashapon || 0;

      result.totalPoint = account.point;
      result.totalGashapon = account.gashapon;
      callback(null, result);
    }).catch(callback);
  }

  Account.observe('loaded', (ctx, next)=>{
    if(ctx.instance && (ctx.instance.exp || ctx.instance.exp == 0)) {
      ctx.instance.lv = new LV().getLv(ctx.instance.exp);
    }
    next();
  });
}