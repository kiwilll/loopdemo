var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Product) {
  RemoteRouting(Product, {only: [
    '@findById',
    '@find',
    '__get__comments',
  ]});

  Product.validatesInclusionOf('status', {in: ['online', 'offline']});

  /*
   * return products whose title / recommendation matches keyword,
   * limit to 10 records, order by id asc
   *
   * @params {String} keyword
   * @params {Object} filter
   * @callback {Function} callback
   */
  Product.search = (keyword, filter, callback) => {
    filter = filter || {};

    delete filter.where;

    keyword && (filter.where = {
      or: [
        { title: { regexp: `/${keyword}/i` } },
        { recommendation: { regexp: `/${keyword}/i` } }
      ]
    });

    Product.find(filter).then(products=>callback(null, products)).catch(callback);
  };

  Product.remoteMethod('search', {
    accepts: [ {
      arg: 'keyword',
      type: 'string',
      http: {source: 'query'},
      description: '搜索关键字'
    }, {
      arg: 'filter',
      type: 'object',
      http: {source: 'query'},
      description: 'Filter defining fields, order, offset, skip, include'
    }],
    http: {verb: 'get'},
    returns: {root: true, type: ['object']}
  });
};
