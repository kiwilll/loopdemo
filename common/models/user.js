var DisableUserRemote = require('../disables/user.js');
var loopback = require('loopback');
var Promise = require('bluebird');
var OauthValidator = require('../../server/libs/oauth-validator.js');
var bunyan = require('bunyan');
var co = require('co');
var LV = require('../../server/libs/lv-calc.js');

module.exports = function(User) {
  const EXPIRE = 30 * 60 * 1000

  let logger = bunyan.createLogger({name: 'ZCM'});

  let Message = loopback.getModel('message');
  // let VCode = loopback.getModel('vcode');
  let Item = loopback.getModel('item');
  let AccessToken = loopback.getModel('AccessToken');

  User.validatesUniquenessOf('nickname', {message: 'nickname is not unique'});
  User.validatesUniquenessOf('qq', {message: 'qq is not unique'});
  User.validatesUniquenessOf('wechat', {message: 'wechat is not unique'});
  User.validatesUniquenessOf('weibo', {message: 'weibo is not unique'});

  DisableUserRemote(User);

  //in android device, if username and password are number
  //then, transform them into strings
  User.beforeRemote('login', function(ctx, instance, next){
    ctx.req.body.username += '';
    ctx.req.body.password += '';
    next();
  });

  User.passport = function(data, service, callback){
    if (!service || !data[service]) {
      let error = new Error('Bad Request');
      error.statusCode = 400;
      return callback(error);
    }

    let oauthValidator = new OauthValidator(service);

    oauthValidator.validate(data.token, data[service]).then(identity=>{
      if (!identity) {
        let error = new Error('Identity not matched');
        error.statusCode = 401;
        return callback(error);
      }

      data.password = service;

      User.findOrCreate({
        where: {
          [service]: data[service]
        }
      }, data).then(user=>{
        user[0].createAccessToken(EXPIRE, (err, token)=>{
          if (err) {return callback(err);}
          token.__data.user = user[0];
          callback(null, token.toJSON());
        });
      }).catch(callback);
    }).catch(err=>{
      console.log(err);
      callback(err);
    });
  }

  User.remoteMethod('passport', {
    accepts: [{
      arg: 'data', type: 'object', required: true, http: { source: 'body' }
    }, {
      arg: 'service', type: 'string', http: { source: 'query' }
    }],
    description: 'Third party login',
    returns: {
      type: 'object', root: true
    }
  });

  User.afterRemote('passport', (ctx, instance, next)=>{
    co(function*(){
      let user = yield User.findById(instance.userId);
      let account = yield user.account.create();
    }).catch(next);
    next();
  });

  User.register = (data, service, cb)=> {

    if (!validateCredential(data)) {
      let defaultError = new Error('login failed');
      defaultError.statusCode = 401;
      defaultError.code = 'LOGIN_FAILED';
      return cb(defaultError);
    };

    if (service && data[service]) {
      data.password = service;
      User.findOrCreate({
        where: {
          [service]: data[service]
        }
      }, data).then(user=>{
        user[0].createAccessToken(EXPIRE, (err, token)=>{
          if (err) {return cb(err);}
          token.__data.user = user[0];
          cb(null, token.toJSON());
        });
      }).catch(cb);
    } else {
      User.create(data).then((user)=>{
        return User.login(data, 'user');
      }).then(token=>{
        cb(null, token.toJSON());
      }).catch(cb);
    }
  };

  // User.beforeRemote('register', (ctx, instance, next)=>{
  //   let to = ctx.req.body.username;
  //   let code = ctx.req.body.code;
  //   let service = ctx.req.query.service;

  //   if (service) {
  //     return next();
  //   }
  //   if (!code) {
  //     let error = new Error('code is required');
  //     error.statusCode = 422;
  //     return next(error);
  //   }

  //   // VCode.findOne({
  //   //   where: {
  //   //     to,
  //   //     value: code,
  //   //     created_at: { gt: Date.now() - EXPIRE}
  //   //   }
  //   // }).then(vcode=>{
  //   //   if (vcode) {
  //   //     vcode.destroy();
  //   //     next();
  //   //   } else {
  //   //     let error = new Error('Validation code not found');
  //   //     error.statusCode = 404;
  //   //     next(error);
  //   //   }
  //   // }).catch(next);

  // });

  User.afterRemote('register', (ctx, instance, next)=>{
    co(function*(){
      let user = yield User.findById(instance.userId);
      let account = yield user.account.create();
    }).catch(next);
    next();
  });

  User.remoteMethod('register', {
    accepts: [{
      arg: 'data', type: 'object', required: true, http: { source: 'body' }
    }, {
      arg: 'service', type: 'string', http: { source: 'query' }
    }],
    description: 'Register a user',
    returns: {
      type: 'object', root: true
    }
  });

  /**
   * nickname
   *
   * @param {[Number]} data a set of ids
   * @param {function} cb callback
   */
  User.nickname = function(data, cb) {
    let query = data.map(id=>({id}));
    User.find({
      where: {
        or: query
      },
      fields: {nickname: 1, id: 1}
    }).then(user=>{
      cb(null, user);
    }).catch(cb);
  };

  User.remoteMethod('nickname', {
    accepts: {
      arg: 'data', type: ['number'], required: true
    },
    http: { verb: 'get'},
    description: 'get user\'s nickname',
    returns: {
      type: 'object', root: true
    }
  });

  User.prototype.follow = function(followeeId, callback) {
    let Follow = loopback.getModel('follow');

    if (this.id === followeeId) {
      let error = new Error('can not follow self');
      error.statusCode = 400;
      return callback(error);
    }

    User.findById(followeeId).then(user=> {
      if(!user) {
        let error = new Error('followee not found');
        error.statusCode = 404;
        return callback(error);
      }
      return this.followees.add(user);
    }).then(follow=>{
      callback(null, follow);
    }).catch(callback);
  };

  User.remoteMethod('follow', {
    accepts: [{
      arg: 'followeeId',
      type: 'number',
      required: true,
      http: {source:　'query'},
      description: '被关注用户Id'
    }],
    isStatic: false,
    description: 'follow a user',
    returns: {
      type: 'object', root: true
    }
  });

  User.afterRemote('prototype.follow', (ctx, instance, next)=>{
    next();
    instance && process.nextTick(()=>{
      Message.create({
        type: 'follow',
        authorId: instance.followerId,
        replyId: instance.followeeId
      }).catch(console.log);
    });
  });

  /*
   *  this remote hook will add a followed property to
   *  indicate whether current logged user followed the user
   */
  User.afterRemote('findById', (ctx, instance, next)=>{
    const Follow = loopback.getModel('follow');
    const loggedUserId = ctx.req.accessToken && ctx.req.accessToken.userId;
    if (loggedUserId) {
      Follow.find({
        where: {followerId: loggedUserId, followeeId: instance.id}
      }).then(follows=>{
        instance.followed = follows.length ? true : false;
        next();
      }).catch(err=>{
        console.log(err);
        next();
      });
    } else {
      instance.followed = false;
      next();
    }
  });

  User.prototype.unfollow = function(followeeId, callback) {
    let Follow = loopback.getModel('follow');

    if (this.id === followeeId) {
      let error = new Error('can not follow self');
      error.statusCode = 400;
      return callback(error);
    }

    User.findById(followeeId).then(user=>{
      if(!user) {
        let error = new Error('followee not found');
        error.statusCode = 404;
        return callback(error);
      }
      return this.followees.remove(user);
    }).then(follow=>{
      callback(null);
    }).catch(callback);
  }

  User.remoteMethod('unfollow', {
    accepts: [{
      arg: 'followeeId', type: 'number', required: true, http:{source: 'query'}
    }],
    http: {verb: 'delete'},
    isStatic: false,
    description: 'Unfollow a user'
  })

  /**
   * reset
   *
   * @param {object} data user credentials
   * data : {
   *  username: 13872747271,
   *  code: 1123,
   *  password: 123
   * }
   * @param {function} callback
   */
  User.reset = function(data, callback) {
    if (!data.password ) {
      let error = new Error('password is required');
      error.statusCode = 400;
      return callback(error);
    }

    // transform numbers to string
    data.username += '';
    data.password += '';

    User.findOne({
      where: {
        username: data.username
      }
    }).then(user=>{
      return user.updateAttributes(data);
    }).then(user=>{
      callback(null, user.toJSON());
    }).catch(callback);
  };

  User.beforeRemote('reset', (ctx, instance, next)=>{
    let to = ctx.req.body.username;
    let code = ctx.req.body.code;
    const EXPIRE = 30 * 60 * 1000

    if (!code) {
      let error = new Error('code is required');
      error.statusCode = 422;
      return next(error);
    }

    // VCode.findOne({
    //   where: {
    //     to,
    //     value: code,
    //     created_at: { gt: Date.now() - EXPIRE}
    //   }
    // }).then(vcode=>{
    //   if (vcode) {
    //     vcode.destroy();
    //     next();
    //   } else {
    //     let error = new Error('Validation code not found');
    //     error.statusCode = 404;
    //     next(error);
    //   }
    // }).catch(next);
    
  });

  User.remoteMethod('reset', {
    accepts: {
      arg: 'data', type: 'object', required: true, http: {source: 'body'}
    },
    http: {verb: 'post'},
    description: 'Reset password',
    returns: {
      type: 'object', root: true
    }
  });

  /**
   * validate_token
   *
   * @param {string} token token id
   * @param {function} callback
   */
  User.validate_token = function(token, callback) {
    AccessToken.findById(token).then(token=>{
      if (token) {
        token.validate((err, isValid)=>{
          if (err){
            return callback(err);
          } else {
            return callback(null, {valid: isValid});
          }
        });
      } else {
        let error = new Error('TOKEN_NOT_FOUND')
        error.statusCode = 404;
        return callback(error);
      }
    }).catch(callback);
  };

  User.remoteMethod('validate_token', {
    accepts: [{
      arg: 'token', type: 'string', required: true, http: {source: 'path'}
    }],
    http: {
      verb: 'get',
      path: '/validate_token/:token'
    },
    description: 'Validate an accessToken',
    returns: {
      type: 'object', root: true
    }
  });

  function validateCredential(credentials) {
    if (credentials.username) {
      return credentials.username && credentials.password;
    } else {
      return credentials.qq || credentials.wechat || credentials.weibo
    }
  };

  /**
   * determine whether current user follows ones' followers
   * 获取用户的粉丝
   */
  User.afterRemote('prototype.__get__followers', (ctx, instance, next)=>{
    const Follow = loopback.getModel('follow');
    const currentUser = ctx.req.accessToken && ctx.req.accessToken.userId;

    if (currentUser) {
      return Promise.resolve(instance).map(follower=>{
        return Follow.count({
          followerId: currentUser,
          followeeId: follower.id
        }).then(count=>{
          follower.followed = count ? true : false;
          return follower;
        });
      }).then(()=>next()).catch(next);
    } else {
      instance = instance.map(follower=>{
        follower.followed = false;
        return follower;
      });

      return next();
    }
  });

  /**
   * determine whether current logged user follows ones' followees
   * 获取用户关注的人
   */
  User.afterRemote('prototype.__get__followees', (ctx, instance, next)=>{
    const Follow = loopback.getModel('follow');
    const currentUser = ctx.req.accessToken && ctx.req.accessToken.userId;

    if(currentUser){
      return Promise.resolve(instance).map(followee=>{
        return Follow.count({
          followerId: currentUser,
          followeeId: followee.id
        }).then(count=>{
          followee.followed = count ? true : false;
          return followee;
        });
      }).then(()=>next()).catch(next);
    } else {
      instance = instance.map(followee=>{
        followee.followed = false;
        return followee;
      });
      next();
    }
  });

  /*
   * return users whose nickname matches keyword, limit to 10 records
   * order by id asc
   *
   * @params {String} keyword
   * @params {Number} skip
   * @callback {Function} callback
   */
  User.search = (keyword, skip, callback)=>{
    let filter = {
      order: ['id ASC'],
      limit: 10,
      skip: skip || 0
    };

    if (keyword) {
      filter.where = {
        nickname: {regexp: `/${keyword}/i`}
      };
    }

    User.find(filter).then(users=> callback(null, users)).catch(err=>{
      logger.error(err);
      return callback(err);
    });
  }

  User.remoteMethod('search', {
    accepts: [ {
      arg: 'keyword',
      type: 'string',
      http: {source: 'query'},
      description: '搜索关键字'
    }, {
      arg: 'skip',
      type: 'number',
      http: {source: 'query'},
      description: '搜索偏移量'
    } ],
    http: {verb: 'get'},
    returns: {root: true, type: ['object']}
  });

  /**
   *  when create an comment on item
   *  set itemId, activityId or figureId for backward capability
   *
   *  if no replyId is provided, then it's directly replied to the item's author
   *  Otherwise the comment is replied to a specific user
   */

  User.beforeRemote('prototype.__create__comments', (ctx, instance, next)=>{
    let {instModel, instId } = ctx.args.data;

    let error = new Error('Missing instId / instModel');
    error.statusCode = 422;

    if (!instId || !instModel) {
      return next(error);
    }

    if (instModel === 'item' || instModel === 'activity' || instModel === 'figure') {
      ctx.args.data[`${instModel}Id`] = instId;
    }

    next();
  });

  User.afterRemote('prototype.__create__comments', function updateItemUpdatedAt(ctx, instance, next){
    next();
    if(instance &&instance.instModel === 'item'){
      instance.item.getAsync().then(item=>{
        item.updateAttributes({
          updated_at: instance.updated_at
        }).catch(console.log)
      });
    };
  });

  /**
   *  create a notification message after comment is created
   */
  User.afterRemote('prototype.__create__comments', (ctx, instance, next) => {
    next();

    notifyAfterComment(instance).catch(console.log);

    function notifyAfterComment(comment) {
      return co(function*() {
        let {instModel, instId, userId, replyId} = comment;

        let message =  {
          type: replyId ? ['reply', instModel].join('_') : 'comment',
          authorId: userId,
          replyId: replyId,
          instId: instId,
          instModel: instModel
        };

        if (instModel === 'item' ||
            instModel === 'activity' ||
            instModel === 'figure') {
          message[`${instModel}Id`] = instId;
        }

        if (comment.replyId && comment.itemId) {
          message.type = 'reply';
        }

        if (!comment.replyId && comment.itemId) {
          message.replyId = yield comment.item.getAsync().then(item=>item.userId);
        }

        return Message.create(message);
      });
    };
  });

  /*
   * 获取当前用户是否点赞过
   */
  User.afterRemote('prototype.__get__items', (ctx, instance, next) => {
    const Thumbsup = loopback.getModel('thumbsup');
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;
    if(!userId) {
      instance.forEach(item=> item.thumbsupOwner = false);
      return next();
    }

    Promise.resolve(instance).map(item=>{
      return Thumbsup.count({itemId: item.id, userId}).then(count=> {
        item.thumbsupOwner = count ? true : false;
        return item;
      });
    }).then(()=>next());
  });

  User.batchUpdateBackground = function(id, data, callback){
    User.findOne({where: {id: id}}).then(user=> {
      return user.backgrounds.destroyAll().then(()=> {
        user.backgrounds.create(data, (err, backgrounds)=>{
          if(err) return callback(err);
          callback(null, backgrounds);
        });
      });
    }).catch(callback);
  };

  User.remoteMethod('batchUpdateBackground', {
    http: {path: '/:id/backgrounds/batch/', verb: 'put', status: 200},
    description: '批量更新背景图',
    accepts: [
      {arg: 'id', type: 'number', http: { source: 'path'}, required: true},
      {arg: 'data', type: '[image]', http: { source: 'body'}, required: true}
    ],
    returns: {root: true,  type: '[image]'}
  });

  //积分相关
  User.afterRemote('prototype.__get__account', (ctx, instance, next)=>{
    const Award = loopback.getModel('award');
    co(function*(){
      if(ctx.instance) {
        instance.mobile = ctx.instance.mobile;
        instance.address = ctx.instance.address;
        instance.addressee = ctx.instance.addressee;
      }
      next();
    }).catch(next);
  });

  User.prototype.attendance_post = function(callback) {
    const Daily = loopback.getModel('daily');
    Daily.attendance(this.id, callback);
  }

  User.remoteMethod('attendance_post', {
    http: {path:'/attendance', verb: 'post', status: 200, errorStatus: 403},
    description: '本日签到',
    isStatic: false,
    returns: {type: "object", root: true}
  });

  User.prototype.attendance_get = function(year, month, callback) {
    const Daily = loopback.getModel('daily');
    Daily.getAttendances(this.id, year, month, callback);
  }

  User.remoteMethod('attendance_get', {
    accepts: [{
      arg: 'year', type: 'number', required: true,
      description: '年份(四位整数), 如2016'
    },{
      arg: 'month', type: 'number', required: true,
      description: '月份, 1-12'
    }],
    http: {path:'/attendance', verb: 'get', status: 200, errorStatus: 403},
    description: '获取签到状态',
    isStatic: false,
    returns: {type: "object", root: true}
  });

  /**
   *  回复后加积分
   */
  User.afterRemote('prototype.__create__comments', (ctx, instance, next) => {
    co(function*() {
      const Daily = User.app.models.daily;
      let result = yield Promise.promisify(Daily.do)(instance.userId, '回复');
      instance.account = result;
      next();
    }).catch(next);
  });

  User.prototype.shared = function(callback) {
    const Daily = User.app.models.daily;
    Daily.do(this.id, '分享', (err, result)=>{
      return callback(err, result);
    });
  }

  User.remoteMethod('shared', {
    http: {path:'/shared', verb: 'get'},
    description: '分享成功后回调，用于积分计算',
    isStatic: false,
    returns: {type: "object", root: true}
  });

  User.prototype.purchased = function(callback) {
    const Daily = User.app.models.daily;
    Daily.do(this.id, '购买', (err, result)=>{
      return callback(err, result);
    });
  }

  User.remoteMethod('purchased', {
    http: {path:'/purchased', verb: 'get'},
    description: '购买成功后回调，用于积分计算',
    isStatic: false,
    returns: {type: "object", root: true}
  });

  User.afterRemote('prototype.__link__thumbsup_items', (ctx, instance, next)=>{
    co(function*(){
      const Daily = loopback.getModel('daily');
      let result = yield Promise.promisify(Daily.do)(instance.userId, '点赞');
      
      ctx.result.result = result;
      return next();
    }).catch(console.log);
  });

  User.afterRemote('prototype.__link__thumbsup_activities', (ctx, instance, next)=>{
    co(function*(){
      const Daily = loopback.getModel('daily');
      let result = yield Promise.promisify(Daily.do)(instance.userId, '点赞');
      
      ctx.result.result = result;
      return next();
    }).catch(console.log);
  });

  User.prototype.getLv = function(callback) {
    let userId = this.id;
    co(function*(){
      const Daily = loopback.getModel('daily');
      let dailys = yield Promise.promisify(Daily.get)(userId);

      const Account = loopback.getModel('account');
      let account = yield Account.findOne({where:{userId: userId}});
      let exp = account.exp;
      
      let Lv = new LV();
      let lvs = Lv.getAll();
      let lv = Lv.getLv(exp);
      callback(null, {dailys: dailys, exp: exp, lv: lv, lvs: lvs});
    }).catch(callback);
  }

  User.remoteMethod('getLv', {
    http: {path:'/getLv', verb: 'get'},
    description: '获得积分相关结果',
    isStatic: false,
    returns: {type: "object", root: true}
  });
};
