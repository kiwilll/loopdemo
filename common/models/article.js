var RemoteRouting = require('loopback-remote-routing');
var Promise = require('bluebird');
var co = require('co');
var loopback = require('loopback');

module.exports = function(Article) {
  RemoteRouting(Article, {only: [
    '@findById'
  ]});
}
