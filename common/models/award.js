var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Award) {
  RemoteRouting(Award, {only: [
    '@find'
  ]});
}