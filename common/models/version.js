var DisableVersionRemote = require('../disables/version.js');

module.exports = function(Version) {

 DisableVersionRemote(Version);

 Version.latest = function(os, callback) {
   Version.findOne({
     where: {os: os},
     order: 'created_at DESC'
   }).then(version=>{
     callback(null, version);
   }).catch(callback);
 }

 Version.remoteMethod('latest', {
   accepts: [
    {arg: 'os', type: 'string', required: true, http: {source: 'query'}}
   ],
   http: {verb: 'get'},
   description: 'return the latest version for ios/android',
   returns:  { type: 'object', root: true }
 })
};
