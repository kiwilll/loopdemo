var RemoteRouting = require('loopback-remote-routing');
var Promise = require('bluebird');
var co = require('co');
var loopback = require('loopback');

module.exports = function(Banner) {
  RemoteRouting(Banner, {only: [
    '@find'
  ]});
}
