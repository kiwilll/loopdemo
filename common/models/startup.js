var DisableStartupRemote = require('../disables/startup.js');

module.exports = function(Startup) {
  DisableStartupRemote(Startup);
};
