var DisableFigureRemote = require('../disables/figure.js');
var qiniu = require('qiniu');
var crypto = require('crypto');

module.exports = function(Figure) {
  DisableFigureRemote(Figure);

  Figure.downloadUrl = function (url, downloadToken, cb) {
    Figure.find({where: {path: url}}).then(figures=> {
      const figure = figures[0];
      if(figure) {
        const reg = /^http:\/\/(\S+)\/(\S+)$/.exec(url);
        let baseUrl = qiniu.rs.makeBaseUrl(reg[1], reg[2]);
        let policy = new qiniu.rs.GetPolicy();
        return cb(null, policy.makeRequest(baseUrl));
      } else {
        const error = new Error('not found');
        error.statusCode = 404;
        return cb(error);
      }
    }).catch(err=> {
      console.log(err);
      return cb(err);
    });
  };
  /**
   * access_token=${authorization}$app=${app}&app-version=${appVersion}
   */
  Figure.beforeRemote('downloadUrl', (ctx, instance, next)=> {
    const {downloadToken} = ctx.req.query;
    const {app, authorization} = ctx.req.headers;
    const appVersion = ctx.req.headers['app-version'];
    const md5 = crypto.createHash('md5');
    const key = md5.update(`access_token=${authorization}&app=${app}&app-version=${appVersion}`).digest('hex');
    if(!downloadToken || key !== downloadToken) {
      const errTxt = !downloadToken ? 'downloadToken is required' : 'downloadToken is invalid';
      let error = new Error(errTxt);
      error.statusCode = 422;
      return next(error);
    };

    return next();
  });

  /*
   * auto increase figure.download by 1 when user downloads a figure
   */
  Figure.afterRemote('downloadUrl', (ctx, instance, next)=>{
    next();
    process.nextTick(()=>{
      return Figure.findOne({where: {path: ctx.req.query.path}}).then(figure=>{
        return figure.updateAttributes({download: ++figure.download});
      }).catch(console.log);
    })
  });

  Figure.remoteMethod('downloadUrl', {
    description: 'Return qiniu downloadUrl',
    accepts: [
      { arg: 'path', type: 'string', http: {source: 'query'} },
      { arg: 'downloadToken', type: 'string', http: {source: 'query'} }
    ],
    http: {verb: 'get'},
    returns: { arg: 'url', type: 'string' }
  });

  /*
   *  FIX: sort figures order by updated_at desc
   *
   *  android(<= v1.2.5): sort by id desc
   *  ios(<= 1.2.2): sort by created_at desc
   *
   */
  Figure.beforeRemote('find', function lastestFigures(ctx, instance, next){

    if (!ctx.req.query.filter) {
      return next();
    }

    let filter = ctx.req.query.filter;

    if (typeof filter === 'string') {
      try {
        ctx.req.query.filter = filter = JSON.parse(filter);
      } catch(e){
        console.log(e);
      }
    }

    if (Array.isArray(filter.order) && filter.order[0] === 'created_at DESC') {
      ctx.req.query.filter.order[0] = 'updated_at DESC';
    }

    if (filter.order === 'id desc' || filter.order === 'created_at DESC') {
      ctx.req.query.filter.order = 'updated_at DESC';
    }

    ctx.args.filter = ctx.req.query.filter;

    next();
  });
};
