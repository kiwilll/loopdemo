var DisableTagRemote = require('../disables/tag.js');

module.exports = function(Tag) {

  Tag.validatesUniquenessOf('name', {message: 'name is not unique'});

  DisableTagRemote(Tag);
};
