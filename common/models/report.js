var DisableReportRemote = require('../disables/report.js');
var Mailer = require('../../server/libs/mail.js');
var Promise = require('bluebird');
var loopback = require('loopback');

// let mailConfig = null;

// switch(process.env.NODE_ENV) {
//   case 'production':
//     mailConfig = require('../../server/config.production.json').submail.mail;
//   break;
//   case 'staging' :
    // mailConfig = require('../../server/config.staging.json').submail.mail;
//   break;
//   default:
//     mailConfig = require('../../server/config.json').submail.mail;
//   break;
// }

// let mailer = new Mailer(mailConfig);

module.exports = function(Report) {
  DisableReportRemote(Report);

  Report.afterRemote('create', (ctx, instance, next)=>{
    next();

    const User = loopback.getModel('user');
    const Item = loopback.getModel('item');

    const env = process.env.NODE_ENV === 'staging' ? '测试环境' : '';

    process.nextTick(()=>{
      Promise.props({
        reporter: User.findById(instance.reporterId),
        item: Item.findById(instance.itemId)
      }).then(result=>{
        const reporter = result.reporter;
        const item = result.item;

        // mailer.send(
        //   `${env}用户${reporter.nickname||''}对${item.id}号内容举报`,
        //   `内容： ${item.content}`, (err, status)=>{
        //     if (err) return console.log(err);
        // })
        
      }).catch(console.log);
    });
  });
};
