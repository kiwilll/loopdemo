var RemoteRouting = require('loopback-remote-routing');

module.exports = function(Recommendation) {
  RemoteRouting(Recommendation, {only: [
    '@__get__recommended_products'
  ]});
};
