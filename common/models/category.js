var DisableCategoryRemote = require('../disables/category.js');
var loopback = require('loopback');
var Promise = require('bluebird');

module.exports = function(Category) {

  Category.validatesUniquenessOf('name', {message: 'name is not unique'});

  DisableCategoryRemote(Category);

  Category.afterRemote('prototype.__get__items', (ctx, instance, next)=> {

    const Thumbsup = loopback.getModel('thumbsup');
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;

    if (userId) {
      Promise.resolve(instance).map(item=>{
        return Thumbsup.count({itemId: item.id, userId}).then(count=> {
          item.thumbsupOwner = count ? true : false;
          return item;
        })
      }).then(()=>next());
    } else {
      instance.forEach(item=> item.thumbsupOwner = false);
      next();
    }
  });

  Category.prototype.getHotTags = function(callback) {
    callback(null, this.hotTags);
  }

  Category.remoteMethod('getHotTags', {
    http: {verb: 'get', status: '200', path: '/hotTags'},
    description: 'Get custom defined hot tags',
    isStatic: false,
    returns: {root: true, type: [String]}
  });
};
